# Guide — `API 0057` — E23

The associated website is available [here](https://api0057.gitlab.utc.fr/api0057-e2023/guide).

## Setup

This guide is built using [docsify](https://github.com/docsifyjs/docsify).
The first thing to do is to make sure you have the `docsify-cli` installed: `npm i docsify-cli -g`.

- `make init` to setup the pre-commits
- `docsify serve docs` or `make run`
- Simply edit the files!
