# Scaleway VM

## Règles

- Utiliser les VMs dans le respect de la charte informatique de l'UTC (interdiction formelle de faire du minage de cryptomonnaies, d'héberger des contenus à caractères pornographique, etc.). Toutes infractions donnera lieu à l'application des sanctions disciplianires adéquates.
- SVP:
  - Ne touchez pas aux machines des autres,
  - Ne cramez pas les crédits de tout le monde,
  - Créez des ressources uniquement dans le projet `students-playground` (vous ne pouvez pas ailleurs de toute manière 😜)
  - Ne modifiez pas les règles de sécurité qui ont été mises en place (Security Group)

## Création de sa VM

Dans le projet `students-playground`, vous pouvez créer au plus 1 VM de type `GP1XS` (pensez à l'éteindre le soir, la planète vous dit merci). Voici le process à suivre.

1. Choisir le bon projet
   ![](assets/select-project.png)

2. Choisir la zone (`PARIS 2` est la zone la plus eco-friendly qui existe, ne nous privons pas !)
   ![](assets/select-zone.png)

3. Choisir le type d'instance (`GP1XS` obligatoire pour tout le monde, merci d'avance)
   ![](assets/select-instance-type.png)

4. Choisir l'OS (Debian 12).
   ![](assets/select-os.png)

5. Choisir le type et la taille du volume principal (Block, 150GB).
   ![](assets/set-volume-type-and-size.png)

6. Indiquer votre login comme nom de l'instance et ajouter votre clef SSH publique de manière adéquate.
   ![](assets/set-vm-name-and-add-ssh-key.png)

7. Créer l'instance !
   ![](assets/create-vm.png)
