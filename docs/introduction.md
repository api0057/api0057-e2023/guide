## Introduction

Nous vous proposons une API pour découvrir (ou redécouvrir) le monde de l'_Applied Data Science_ au travers des thématiques de _Software-Engineering_ et de _Data-Engineering_, avec un fil rouge autour des donnés géographiques.

Quelques mots-clés pour vous donner envie ou attiser votre curiosité : Git, Linux, Cloud, SSH, Python, tests, Structures de données (oui ça sert vraiment dans la vraie vie et pas que en NF16), Haute-Volumétrie, traitements en streaming, Map Reduce / Apache Spark, Dagster, sans oublier PostgreSQL et PostGIS.

**Il va de soi que vous ne serez jamais des experts de tout cela en 5 jours, mais il s'agit bien de vous donner des billes et des réflexes pour votre future vie professionnelle.**

Cerise sur le gâteau, nous devrons être entre 2 et 3 encadrants à temps plein pour vous accompagner tout au long de cette API avec un mix d'un Enseignant-Chercheur et d'anciens UTCéens depuis quelques années dans la vie professionnelle. Ie, ça sera du quasi-bénévolat, mais ça sera pour le plaisir de transmettre et on est très motivé. Donc venez si vous l'êtes aussi !

## Objectifs pédagogiques

Objectif de l’Api : L'objet de cette API est de fournir aux étudiants des connaissances et compétences de base dans la mise en œuvre du traitement de données, en particulier dans le cas de données massives.

Objectifs spécifiques :

- Ne pas être démuni sur des techniques modernes de traitement de données (Python, Spark, Streaming, SQL)
- Initiation au développement computationnellement efficace d'algos de traitements de données
- Découverte d'outils d'organisation de pipeline de calculs et d'observabilité

Objectifs transversaux :

- Mise en pratique de bonnes pratiques de dev (git, ssh, tests, _etc._) et prise en main du _« cloud »_.
