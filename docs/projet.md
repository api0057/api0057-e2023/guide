# Projet

Objectifs pédagogiques :

- [ ] Exploration de données en autonomie
- [ ] Proposition de traitements techniques adéquats et sensés
- [ ] Ie, développer et conforter les compétences acquises pendant l'API

## Rendu

Soutenance (chill), tout le monde assiste de 16h à 18h!

Par sujet (4 mins):

- Présentation collective du sujet / de la question
- Présentation collective des jeux de données utilisées

Pour chaque groupe :

- 8 mins de présentation
  - Présentation de l'approche
  - Présentation du code
  - Présentation des résultats
- 5 mins de question

## Technologies

- a. PostreSQL / PostGis + traitements python
- b. Spark + traitements python

## Questions

1. Existe-t-il un lien entre résultats électoraux & niveau d'étude / niveau de chômage / (autre caractéristique socio-économique de votre choix) ?
2. Proposer une cartographie des projections climatiques à l'échelle communale ?
3. Quelles sont les zones sur/sous-dotées en bornes de recharge électrique compte tenu de la densité de population ?
4. La localisation des antennes relais tient-elle compte de la localisation des écoles ?

## Groupes

- 7 groupes de ~4 personnes, en présentiel
- Tirage des au sort de la combinaison sujet & technologie :
  - a1
  - b1
  - a2
  - b2
  - a3
  - b3
  - a4
  - b4
