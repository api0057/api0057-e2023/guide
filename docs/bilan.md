# À retenir $-$ API Applied Data Science

## Compétences générales en informatique

### Shell

- Utiliser la touche `tab` 🙏
- Linux ❤️
- ZSH / Oh-my-zsh
- Configuration de son shell avec `.zshrc`
- Création d'un utilisateur, etc.
- Les commandes `top`, `htop`, `tmux`, `pkill`, etc.

### SSH

- Génération de clefs SSH
- SSH d'une machine distante (depuis Windows)
- Configuration de gitlab
- Tunnel SSH

### Git

- Connaissances basiques sur le fonctionnement de git
- Les commandes de bases
- La notion de _pre-commit_
- Faire une PR / MR
- Création de repo

## Software Engineering

### Python / dev en général

- Packaging & publication de package python
- Notions d'environnements virtuels
- Nommage des variables et bonnes pratiques de dev (documention)
- Logging vs Print
- Usage des `dataclass` (`frozen`)
- Mise en place de CLI
- Passage de _secrets_ en variable d'environnement
- Immutabilité
- `black` pour formatter le code
- `isort` pour trier les imports
- `pylint` pour _linter_ le code
- `poetry` pour gérer ses dépendances et les _locker_
- Utilisation d'Ipython et des Jupyter notebook
- Utilisation d'un IDE (`VScode`)

### Tests

- TDD : vous savez ce que c'est.
- Quand écrire des tests ?
- Coverage d'un programme

## DevOps

### Docker

- Découverte des _notions_ d'images, de container, etc.
- Prise en main de Docker et de docker-compose (volume, ports)
- Création de Dockerfile
- Introduction à Kubernetes

### Git (suite)

- Mise en place de CI (_Continuous Integration_ -- build & tests) / CD (_Continous Delivery_ -- publication de package, d'images docker) Gitlab

### Cloud Computing

- Découverte de l'interface et usage d'un provider Cloud
- Introduction aux services et spécificités des providers Cloud
- Déploiment et configuration de VM
- Scaleway :fire:

## Data Engineering / Data Analysis & Programmation scientifique

### Programmation efficace

- Structures de données
- Analyse de complexité
- Utilisation de l'écosystème numérique Python & vectorisation de calcul
- Le GIL en Python
- Multi-processing VS Multi-threading
- CPU-Bound vs IO-Bound

### Real World Data

- Pré-traitements / nettoyage des données
- Interventions de Thibaud et Florent
- Formats de stockage Avro vs Parquet vs CSV vs JSON
- Chercher de la donnée en Open Data

### PostgreSQL / Postgis

- Insertion de données depuis Python avec `psycopg`
- Insertion de données avec `COPY`
- Découverte et utiliastion des CTE (notion de `MATERIALIZED`)
- _Prepared Statesment_ avec les paramètres en dehors du SQL
- Utilisation de `psql`
- Indexation et influence sur les performances
- Examen des plans d'execution
- Savoir que l'on peut tunner sa configuration PostgreSQL

### Streaming

- Traitements de données en $\operatorname{O}(1)$ / $\operatorname{O}(\log(n))$ en mémoire

### Spark

- Concepts généraux de Mapping / Reducion
- Utilisation de l'API RDD
- Utilisation de l'API Spark SQL
- Définition de ses propres UDFs (User Defined Functions)
- Spark peut être plus lent que un seul thread `Python`

### Gestion de Pipeline

- Connaissance de l'existance d'Airflow / dagster

## Geographic Data

- Vous savez que la BAN existe
- Les différents types de géométries que vous pouvez traiter
- Connaissance des formats de stockage Geojson et Shapefile
- Manipulation basique de données géographiques avec `shapely`
- Les projections de données géographiques (WGS84 / 4326, Lambert 93 / 2154, etc.)
- Une pointe de Qgis pour visualiser les données
