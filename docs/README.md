# Guide — `API 0057` — E23

## Organisation

- **Commmunication : [Mattermost, _API0057 E23 - Applied Data Science_](https://team.picasoft.net/signup_user_complete/?id=u4p1jfhs6pn8pkaix6pkzqntja&md=link&sbr=su)**
- Modalité : Présentiel
- Lieu : BF A321
- Horaires :
  - 9h début des hostilités
  - 13h -> 14h30 Pause déjeuner pour faire le plein d'énergie
  - 18h30 fin théorique
- Amener son 💻
- [Une VM individuelle chez Scaleway 🔥](./scaleway-vm/README.md)

## Encadrement

- Jean-Benoist Léger — Enseignant-Chercheur [@UTC](https://www.utc.fr/)
- Florent Chehab, GI UTC FDD 2019 — Data / R&D Team Lead | DPO [@Qomon](https://qomon.com/) — [Linkedin](https://www.linkedin.com/in/flochehab/)
- Thibaud Le Graverend, GI UTC IAD 2022 — DataEng & DevOps @FranceTV — [Linkedin](https://www.linkedin.com/in/thibaud-le-graverend/).

## Planning détaillé

- [Lundi](./planning/1.lundi.md)
- [Mardi](./planning/2.mardi.md)
- [Mercredi](./planning/3.mercredi.md)
- [Jeudi](./planning/4.jeudi.md)
- [Vendredi](./planning/5.vendredi.md)

## Projet

[Document de référence](./projet.md)

## Introduction (rappel)

[Ici.](./introduction.md)

## Bilan

[Ici.](./bilan.md)

## Remerciements

- [Scaleway](www.scaleway.com) pour la mise à disposition de crédits 🚀
