SHELL = /bin/bash

.DEFAULT_GOAL := all

## help: Display list of commands
.PHONY: help
help: Makefile
	@sed -n 's|^##||p' $< | column -t -s ':' | sed -e 's|^| |'

## all: Run all targets
.PHONY: all
all: init style

## init: Bootstrap app.
.PHONY: init
init:
	-pre-commit install -t pre-commit -t commit-msg --install-hooks

## run: Run docsify.
.PHONY: run
run:
	docsify serve docs

## style: Check lint, code styling rules.
.PHONY: style
style:
	pre-commit run -v --all-files --show-diff-on-failure

## format: Check lint, code styling rules.
.PHONY: format
format:
	pre-commit run -v --all-files --show-diff-on-failure

## clean: Remove temporary files
.PHONY: clean
clean:
	-pre-commit uninstall -t pre-commit -t commit-msg
